#include "minmaxai.h"


namespace AIs {



MinMaxAI::MinMaxAI(const othello::PlayerId&)
{
    // from http://dhconnelly.com/paip-python/docs/paip/othello.html
    values = {120, -20,  20,   5,   5,  20, -20, 120,
              -20, -40,  -5,  -5,  -5,  -5, -40, -20,
              20,  -5,  15,   3,   3,  15,  -5,  20,
              5,  -5,   3,   3,   3,   3,  -5,   5,
              5,  -5,   3,   3,   3,   3,  -5,   5,
              20,  -5,  15,   3,   3,  15,  -5,  20,
              -20, -40,  -5,  -5,  -5,  -5, -40, -20,
              120, -20,  20,   5,   5,  20, -20, 120
             };
}

othello::BitPos MinMaxAI::bestDepthOneMove(othello::BitBoard board)
{
    if(root.getChildren().empty()){
        root = BitBoardNode(board);
        root.setChildren(m_legalMoves, m_playerId);
    }
    return highestValueMove(root.getChildren());

}

othello::BitPos MinMaxAI::evaluateNextLevel(std::vector<BitBoardNode> nodes)
{
    std::vector<BitBoardNode> enemyMoves;
    for(auto node : nodes) {
        if(node.getChildren().empty()) {
            othello::BitPosSet legalmoves = othello::utility::legalMoves(node.getBoard(),
                                                                         othello::PlayerId((size_t(m_playerId) + 1) %2));
            node.setChildren(legalmoves, othello::PlayerId((size_t(m_playerId) + 1) %2));
        }
        int highestValue = -100000;
        BitBoardNode bestEnemyMove;
        for(auto enemyNode : node.getChildren()) {
            int pieces = 0;
            int otherpieces = 0;
            othello::BitBoard currentBoard = enemyNode.getBoard();
            for(auto i = 0; i != currentBoard.size(); ++i) {
                if(othello::utility::occupied(currentBoard[(size_t(m_playerId) + 1) % 2], othello::BitPos(i)))
                    pieces += values.at(i);
                else if(othello::utility::occupied(currentBoard[size_t(m_playerId)], othello::BitPos(i)))
                    otherpieces += values.at(i);
            }
            int value = pieces - otherpieces;
            if(value > highestValue) {
                highestValue = value;
                bestEnemyMove = enemyNode;
            }
        }
        enemyMoves.push_back(bestEnemyMove);
    }
}

othello::BitPos MinMaxAI::highestValueMove(std::vector<BitBoardNode> nodes)
{

    int highestValue = -10000;
    for(auto node : nodes ) {
        int nPlayerpieces = 0;
        int nOpponentPieces = 0;
        othello::BitBoard currentBoard = node.getBoard();
        for(auto i = 0; i != currentBoard.size(); ++i) {
            if(othello::utility::occupied(currentBoard[size_t(m_playerId)], othello::BitPos(i)))
                nPlayerpieces += values.at(i);
            else if(othello::utility::occupied(currentBoard[(size_t(m_playerId) + 1) % 2], othello::BitPos(i)))
                nOpponentPieces += values.at(i);
        }
        int value = nPlayerpieces - nOpponentPieces;
        if(value > highestValue)
            highestValue = value;
    }
    return othello::BitPos(highestValue);
}

void MinMaxAI::think(const othello::BitBoard &board, const othello::PlayerId &player_id, const std::chrono::seconds &max_time)
{
    m_legalMoves = othello::utility::legalMoves(board, player_id);
    m_playerId = player_id;

    m_best_move = bestDepthOneMove(board);

    const auto time_start = std::chrono::steady_clock::now();

}

othello::BitPos MinMaxAI::bestMove() const
{
    return m_best_move;
}


} //end namespace AIs
