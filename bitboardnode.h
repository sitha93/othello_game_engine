#ifndef BITBOARDNODE_H
#define BITBOARDNODE_H


#include <vector>
#include "basic_types.h"
#include "utility.h"

class BitBoardNode
{
public:
    BitBoardNode();
    BitBoardNode(othello::BitBoard cboard);
    BitBoardNode(othello::BitBoard cboard, othello::BitPos cmove, BitBoardNode cparent);

    std::vector<BitBoardNode> getChildren();
    void setChildren(othello::BitPosSet legalMoves, othello::PlayerId playerId);

    othello::BitBoard getBoard();

private:
    std::vector<BitBoardNode> children;
    othello::BitBoard board;
    othello::BitPos move;
    int points;
    BitBoardNode *parent;
};

#endif // BITBOARDNODE_H
