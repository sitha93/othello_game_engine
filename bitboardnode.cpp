#include "bitboardnode.h"

BitBoardNode::BitBoardNode()
{

}

BitBoardNode::BitBoardNode(othello::BitBoard cboard)
{
    board = cboard;
}

BitBoardNode::BitBoardNode(othello::BitBoard cboard, othello::BitPos cmove, BitBoardNode* cparent)
{
    move = cmove;
    board = cboard;
    parent = cparent;
}

std::vector<BitBoardNode> BitBoardNode::getChildren()
{
    return children;
}

BitBoardNode* BitBoardNode::getParent()
{
    return parent;
}

void BitBoardNode::setChildren(othello::BitPosSet legalMoves, othello::PlayerId playerId)
{
    for(auto nextMove : legalMoves) {
        othello::BitBoard nextBoard = board;
        othello::utility::placeAndFlip(nextBoard, playerId, nextMove);
        children.push_back(BitBoardNode(nextBoard, nextMove, this));
    }
}

othello::BitBoard BitBoardNode::getBoard()
{
    return board;
}
