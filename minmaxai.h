#ifndef MINMAXAI_H
#define MINMAXAI_H

#include <player_interface.h>
#include "utility.h"
#include "bitboardnode.h"


namespace AIs {




class MinMaxAI : public othello::AIInterface
{
public:
    MinMaxAI(const othello::PlayerId&);

public:
    void   think(const othello::BitBoard& board, const othello::PlayerId& player_id,
                 const std::chrono::seconds& max_time);
    othello::BitPos bestMove() const;

private:
    othello::BitPosSet m_legalMoves;
    othello::PlayerId m_playerId;
//    int depth;
    mutable othello::BitPos     m_best_move;
    BitBoardNode root;
    std::array<int, 64> values;

    othello::BitPos bestDepthOneMove(othello::BitBoard board);
    othello::BitPos evaluateNextLevel(std::vector<BitBoardNode> nodes);
    othello::BitPos highestValueMove(std::vector<BitBoardNode> nodes);

};

#endif // MINMAXAI_H


} // end namespace AIs
