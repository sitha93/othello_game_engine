#include "utility.h"

// stl
#include "numeric"
#include "iostream"
#include <functional>

#include <iostream>

namespace othello::utility
{
  ////////////////////
  //
  //
  // Interface Utility
  // Functions
  //
  //
  ////////////////////

  BitPieces occupiedPositions(const BitBoard& board)
  {
 /*     BitPieces pieces;

      pieces = std::accumulate(std::begin(board),std::end(board),BitPieces{},std::bit_or<BitPieces>());

      return pieces; */

      BitPieces occ;
      for(BitPieces pieces : board) {
          occ |= pieces;
      }
      return occ;
  }

  bool occupied(const BitPieces& pieces, const BitPos& board_pos)
  {
//      return pieces.test(board_pos.value());
      if(pieces[board_pos.value()] == 1) {
          return true;
      } else {
          return false;
      }
 //     return true;
  }

  bool occupied(const BitBoard& board, const BitPos& board_pos)
  {
     /* if(board[0].test(board_pos.value()))
          return true;
      else
          return board[1].test(board_pos.value()); */

      bool occ = false;
      for(BitPieces pieces : board) {
          if(occupied(pieces, board_pos)) {
              occ = true;
          }
      }

      return occ;
  }

  BitPos nextPosition(const BitPos& board_pos, const MoveDirection& dir)
  {
      int val = board_pos.value();

      BitPos pos;

      switch (dir) {
        case othello::MoveDirection::N:
          if(val > 55)
              pos = BitPos::invalid();
          else
              pos = BitPos(val + 8);
          break;

      case othello::MoveDirection::NE:
          if(val > 55 || val % 8 == 0)
              pos = BitPos::invalid();
          else
              pos = BitPos(val + 7);
          break;
      case othello::MoveDirection::E:
          if(val % 8 == 0)
              pos = BitPos::invalid();
          else
              pos = BitPos(val - 1);
          break;
      case othello::MoveDirection::SE:
          if(val <8 || (val + 1) % 8 == 0)
              pos = BitPos::invalid();
          else
              pos = BitPos(val - 9);
          break;
      case othello::MoveDirection::S:
          if(val < 8 )
              pos = BitPos::invalid();
          else
              pos = BitPos(val -8);
          break;
      case othello::MoveDirection::SW:
          if (val < 8 || (val + 1) % 8 == 0)
              pos = BitPos::invalid();
          else
              pos = BitPos(val -7);
          break;
      case othello::MoveDirection::W:
          if((val +1) % 8 == 0)
              pos = BitPos::invalid();
          else
              pos = BitPos(val +1);
          break;
      case othello::MoveDirection::NW:
          if(val > 55 || (val+1) % 8 == 0)
              pos = BitPos::invalid();
          else
              pos = BitPos(val + 9);
          break;
      }

      return pos;
  }

  BitPos findBracketingPiece(const BitBoard& board,
                             const BitPos& board_pos,
                             const PlayerId& player_id,
                             const MoveDirection& dir)
  {
      BitPos next = nextPosition(board_pos, dir);
      if (next.isValid()) {
          if(occupied(board[(size_t(player_id) + 1) % 2], next)){
              while(occupied(board[(size_t(player_id) + 1) % 2], next) && next.isValid()){
                  next = nextPosition(next, dir);
              }
              if(occupied(board[size_t(player_id)], next))
                  return next;
          }
      }
      return BitPos::invalid();
  }

  BitPosSet legalMoves(const BitBoard& board, const PlayerId& player_id)
  {
      BitPosSet set;
      for(size_t i = 0 ; i < detail::computeBoardSize() ; i++) {
//          std::cout << "checking pos: " + std::to_string(i) << std::endl;
          if(isLegalMove(board, player_id, BitPos(i))){
              set.insert(BitPos(i));
          }
      }
 //     std::cout << "returning set of legal moves" << std::endl;
      return set;
  }

  bool isLegalMove(const BitBoard& board, const PlayerId& player_id,
                   const BitPos& board_pos)
  {
        if(!occupied(board, board_pos)){
            std::array<othello::MoveDirection, 8> directions = {othello::MoveDirection::E, othello::MoveDirection::N,
                                                               othello::MoveDirection::NE, othello::MoveDirection::NW,
                                                               othello::MoveDirection::S, othello::MoveDirection::SE,
                                                               othello::MoveDirection::SW, othello::MoveDirection::W};
            for(auto dir : directions) {
                if(findBracketingPiece(board, board_pos, player_id, dir).isValid()){
                    return true;
                }
            }

        }else
            return false;
        return false;
   }


  void placeAndFlip(BitBoard& board, const PlayerId& player_id,
                    const BitPos& board_pos)
  {

      if(isLegalMove(board, player_id, board_pos)) {

          board[size_t(player_id)].set(board_pos.value(), true);
          std::array<othello::MoveDirection, 8> directions = {othello::MoveDirection::E, othello::MoveDirection::N,
                                                             othello::MoveDirection::NE, othello::MoveDirection::NW,
                                                             othello::MoveDirection::S, othello::MoveDirection::SE,
                                                             othello::MoveDirection::SW, othello::MoveDirection::W};
          for(auto dir: directions) {
              BitPos pos = findBracketingPiece(board, board_pos, player_id, dir);
              if(pos.isValid()) {
                  BitPos next = nextPosition(board_pos, dir);

                  while(next.value() != pos.value()) {
                      board[(size_t(player_id) + 1) %2].set(next.value(), false);
                      board[size_t(player_id)].set(next.value(), true);
                      next = nextPosition(next, dir);
                  }
              }

          }

      }


  }

}   // namespace othello::utility
