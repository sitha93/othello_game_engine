#include "engine.h"

#include <iostream>

namespace othello
{

bool OthelloGameEngine::initNewGame()
{
    clearGame();
    m_board[0].set(36, true);
    m_board[0].set(27, true);
    m_board[1].set(35, true);
    m_board[1].set(28, true);
    return true;
}

void OthelloGameEngine::clearGame() {
    BitPieces pieces = std::bitset<64>();
    currentPlayer = PlayerId::One;
    m_board[0] = pieces;
    m_board[1] = pieces;
}

bool OthelloGameEngine::performMoveForCurrentHuman(const BitPos& board_pos)
{
    if(utility::isLegalMove(m_board, currentPlayer, board_pos)) {
        utility::placeAndFlip(m_board, currentPlayer, board_pos);
        currentPlayer = PlayerId((size_t(currentPlayer) + 1) % 2);
        return true;
    } else
        return false;

}

bool OthelloGameEngine::legalMovesCheck(BitBoard board, PlayerId playerId)
{
//    std::cout << "checking legal moves" << std::endl;
    BitPosSet legalMoves = utility::legalMoves(board, playerId);
    std::cout << std::to_string(legalMoves.size()) + " legal moves"<< std::endl;
    if(legalMoves.size() == 0)
        return true;
    else
        return false;
}

void OthelloGameEngine::skipTurn()
{
    currentPlayer = PlayerId((size_t(currentPlayer) + 1) % 2);
}

bool OthelloGameEngine::gameover()
{
    bool one = legalMovesCheck(m_board, PlayerId::One);
    bool two = legalMovesCheck(m_board, PlayerId::Two);
    if(one == 0 && two)
        return true;
    else
        return false;

}

int OthelloGameEngine::countPoints(const PlayerId &player_id)
{
    int points = 0;

    for(int i = 0; i < 64; i++){
        if(utility::occupied(m_board[size_t(player_id)], BitPos(i)))
            points++;
    }
    return points;
}

void OthelloGameEngine::think(const std::chrono::seconds& time_limit )
{

    ai.think(m_board, currentPlayer, time_limit);
    BitPos pos = ai.bestMove();

    utility::placeAndFlip(m_board, currentPlayer, pos);

    currentPlayer = PlayerId((size_t(currentPlayer) + 1) % 2);
}

PlayerId OthelloGameEngine::currentPlayerId() const
{
    return currentPlayer;
}

PlayerType OthelloGameEngine::currentPlayerType() const
{
    if(size_t(currentPlayer) == 0)
        return m_player_one.type;
    else
        return m_player_two.type;
}

BitPieces OthelloGameEngine::pieces(const PlayerId& player_id) const
{
    if(player_id == PlayerId::One)
        return m_board[0];
    else
        return m_board[1];
}

const BitBoard& OthelloGameEngine::board() const { return m_board;}


}   // namespace othello
