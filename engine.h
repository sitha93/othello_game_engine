#ifndef ENGINE_H
#define ENGINE_H



#include "utility.h"

#include <basic_types.h>
#include <engine_interface.h>

#include "orangemonkey_ai.h"

#include <memory>

namespace othello
{

  class OthelloGameEngine : public othello::GameEngineInterface {

    // GameEngineInterface interface
  public:
    bool initNewGame() override;
    void clearGame() override;
    bool performMoveForCurrentHuman(const othello::BitPos&) override;
    bool legalMovesCheck(BitBoard board, PlayerId playerId);
    void skipTurn();
    bool gameover();
    int countPoints(const PlayerId& player_id);

    void                think(const std::chrono::seconds&) override;
    othello::PlayerId   currentPlayerId() const override;
    othello::PlayerType currentPlayerType() const override;
    othello::BitPieces  pieces(const othello::PlayerId&) const override;

    const othello::BitBoard& board() const override;

  private:
    PlayerId currentPlayer;
    PlayerType cPlayerType;
    othello::monkey_ais::OrangeMonkeyAI ai = othello::monkey_ais::OrangeMonkeyAI(PlayerId(1));



  };

} // namespace othello
#endif   // ENGINE_H
